﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SAE2.Models
{
    public class Student
    {
        public int ID { get; set; }
        public string Apellido { get; set; }
        public string Nombre { get; set; }
        public DateTime FechaInscripcion { get; set; }

        public virtual ICollection<Enrollment> Enrollments { get; set; }

    }
}