﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace SAE2.Models
{
    public class Course
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CourseID { get; set; }
        public string NombreCurso { get; set; }
        public int Creditos { get; set; }

        public virtual ICollection<Enrollment> Enrollments { get; set; }

    }
}