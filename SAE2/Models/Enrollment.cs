﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SAE2.Models
{
    public enum Calif
    {
        A,B,C,D,F
    }
    public class Enrollment
    {
        public int EnrollmentID { get; set; }
        public int CourseID { get; set; }
        public int IDEstudiante { get; set; }
        public Calif? Calif { get; set; }

        public virtual Course Course { get; set; }
        public virtual Student Student { get; set; }

    }
}