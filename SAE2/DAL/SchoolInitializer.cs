﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using SAE2.Models;



namespace SAE2.DAL
{
    public class SchoolInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<SchoolContext>
    {
        protected override void Seed(SchoolContext context)
        {
            var students = new List<Student>
            {
            new Student{Nombre="Carson",Apellido="Alexander",FechaInscripcion=DateTime.Parse("2005-09-01")},
            new Student{Nombre="Meredith",Apellido="Alonso",FechaInscripcion=DateTime.Parse("2002-09-01")},
            new Student{Nombre="Arturo",Apellido="Anand",FechaInscripcion=DateTime.Parse("2003-09-01")},
            new Student{Nombre="Gytis",Apellido="Barzdukas",FechaInscripcion=DateTime.Parse("2002-09-01")},
            new Student{Nombre="Yan",Apellido="Li",FechaInscripcion=DateTime.Parse("2002-09-01")},
            new Student{Nombre="Peggy",Apellido="Justice",FechaInscripcion=DateTime.Parse("2001-09-01")},
            new Student{Nombre="Laura",Apellido="Norman",FechaInscripcion=DateTime.Parse("2003-09-01")},
            new Student{Nombre="Nino",Apellido="Olivetto",FechaInscripcion=DateTime.Parse("2005-09-01")}
            };



            students.ForEach(s => context.Students.Add(s));
            context.SaveChanges();
            var courses = new List<Course>
            {
            new Course{CourseID=1050,NombreCurso="Chemistry",Creditos=3,},
            new Course{CourseID=4022,NombreCurso="Microeconomics",Creditos=3,},
            new Course{CourseID=4041,NombreCurso="Macroeconomics",Creditos=3,},
            new Course{CourseID=1045,NombreCurso="Calculus",Creditos=4,},
            new Course{CourseID=3141,NombreCurso="Trigonometry",Creditos=4,},
            new Course{CourseID=2021,NombreCurso="Composition",Creditos=3,},
            new Course{CourseID=2042,NombreCurso="Literature",Creditos=4,}
            };
            courses.ForEach(s => context.Courses.Add(s));
            context.SaveChanges();
            var enrollments = new List<Enrollment>
            {
            new Enrollment{IDEstudiante=1,CourseID=1050,Calif=Calif.A},
            new Enrollment{IDEstudiante=1,CourseID=4022,Calif=Calif.C},
            new Enrollment{IDEstudiante=1,CourseID=4041,Calif=Calif.B},
            new Enrollment{IDEstudiante=2,CourseID=1045,Calif=Calif.B},
            new Enrollment{IDEstudiante=2,CourseID=3141,Calif=Calif.F},
            new Enrollment{IDEstudiante=2,CourseID=2021,Calif=Calif.F},
            new Enrollment{IDEstudiante=3,CourseID=1050},
            new Enrollment{IDEstudiante=4,CourseID=1050,},
            new Enrollment{IDEstudiante=4,CourseID=4022,Calif=Calif.F},
            new Enrollment{IDEstudiante=5,CourseID=4041,Calif=Calif.C},
            new Enrollment{IDEstudiante=6,CourseID=1045},
            new Enrollment{IDEstudiante=7,CourseID=3141,Calif=Calif.A},
            };
            enrollments.ForEach(s => context.Enrollments.Add(s));
            context.SaveChanges();
        }
    }
}